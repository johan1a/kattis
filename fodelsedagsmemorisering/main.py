#!/usr/bin/python
import sys
from collections import defaultdict

lines = []
for line in sys.stdin:
    lines.append(line.strip())
nbr_tests = int(lines[0])

n = lines[0]

friends = defaultdict(lambda: {'score': -1})
for i in range(1, len(lines)):
    (name, score, date) = lines[i].strip().split()
    score = int(score)
    if friends[date]['score'] < score:
        friends[date] = {'name': name, 'score': score}

print(len(friends))
friend_list = sorted(friends, key=lambda date: friends[date]['name'])
for date in friend_list:
    print(friends[date]['name'])
