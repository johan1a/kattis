#!/usr/bin/python
import sys
from collections import deque

lines = []
for line in sys.stdin:
    lines.append(line.strip())

nbr_rounds = int(lines[0])
rounds = []
i = 1
while i < len(lines):
    k = int(lines[i])
    priests = []
    for j in range(i + 1, i + 1 + k):
        priests.append([int(x) for x in lines[j].split()])
    rounds.append(priests)
    i += k + 1

assert nbr_rounds == len(rounds)

transitions = {
    0: {1, 2, 4},
    1: {0, 3, 5},
    2: {0, 6, 3},
    3: {7, 1, 2},
    4: {0, 6, 5},
    5: {7, 1, 4},
    6: {7, 4, 2},
    7: {3, 5, 6},
}


def maximize(preferences, state):
    possible = transitions[state]
    best = min(possible, key=lambda k: preferences[k])
    return min(possible, key=lambda k: preferences[k])

cache = {}

def vote(priests, current, state):
    if (current, state) in cache:
        return cache[(current, state)]

    preferences = priests[current]
    if current == len(priests) - 1:
        best = maximize(preferences, state)
        cache[(current, state)] = best
        return best
    results = []
    for possibility in transitions[state]:
        results.append(vote(priests, current + 1, possibility))

    best = min(results, key=lambda k: preferences[k])
    cache[(current, state)] = best
    return best


def to_string(state):
    return [
        'NNN',
        'NNY',
        'NYN',
        'NYY',
        'YNN',
        'YNY',
        'YYN',
        'YYY',
    ][state]


for priests in rounds:
    cache = {}
    print(to_string(vote(priests, 0, 0)))
