#!/usr/bin/python
import sys
from collections import defaultdict

lines = []
for line in sys.stdin:
    lines.append(line.strip())

i = 0
while i < len(lines) and lines[i] != '-1':
    n = int(lines[i])
    elapsed = 0
    distance = 0
    for j in range(i + 1, i + 1 + n):
        (speed, new_elapsed) = [int(k) for k in lines[j].split()]
        duration = new_elapsed - elapsed
        distance += duration * speed
        elapsed = new_elapsed
    print("{} miles".format(distance))

    i += n + 1

