#!/usr/bin/python
import sys
from collections import deque
import math

lines = []
for line in sys.stdin:
    lines.append(line.strip().replace(' ', ''))


def parse_num(line, pos):
    num = []
    neg = 1
    if line[pos] == '-':
        neg = -1
        pos += 1
    while pos < len(line) and numeric(line[pos]):
        num.append(line[pos])
        pos += 1
    return int("".join(num)) * neg, pos


def numeric(char):
    return char >= '0' and char <= '9'


def parse(line, vals, pos):
    while pos < len(line):
        next = line[pos]
        if next == '-':
            if len(vals) == 0 or vals[-1] in {'-', '+', '*', '/', '('}:
                num, pos = parse_num(line, pos)
                vals.append(num)
            else:
                vals.append('-')
                pos += 1
        elif numeric(next):
            num, pos = parse_num(line, pos)
            vals.append(num)
        elif next == '+':
            vals.append('+')
            pos += 1
        elif next == '*':
            vals.append('*')
            pos += 1
        elif next == '/':
            vals.append('/')
            pos += 1
        elif next == '(':
            vals.append('(')
            pos += 1
        elif next == ')':
            vals.append(')')
            pos += 1
    return vals


def evaluate(vals, pos):
    temp = deque()
    while pos < len(vals):
        char = vals[pos]
        if char == '*':
            if vals[pos + 1] == '(':
                result, pos = evaluate(vals, pos + 2)
            else:
                result = vals[pos + 1]
                pos += 2

            temp.append(temp.pop() * result)
        elif char == '/':
            if vals[pos + 1] == '(':
                result, pos = evaluate(vals, pos + 2)
            else:
                result = vals[pos + 1]
                pos += 2
            temp.append(temp.pop() / result)
        elif char == '(':
            result, extra = evaluate(vals, pos + 1)
            temp.append(result)
            pos = extra
        elif char == ')':
            res, _ = evaluate(temp, 0)
            return res, pos + 1
        else:
            temp.append(vals[pos])
            pos += 1

    pos = 0
    vals = temp
    temp = deque()
    while pos < len(vals):
        char = vals[pos]
        if char == '+':
            if vals[pos + 1] == '(':
                result, pos = evaluate(vals, pos + 2)
            else:
                result = vals[pos + 1]
                pos += 2

            temp.append(temp.pop() + result)
        elif char == '-':
            if vals[pos + 1] == '(':
                result, pos = evaluate(vals, pos + 2)
            else:
                result = vals[pos + 1]
                pos += 2

            temp.append(temp.pop() - result)
        elif char == '(':
            return evaluate(vals, pos + 1)
        elif char == ')':
            assert len(temp) == 1
            return temp.pop(), pos + 1
        else:
            temp.append(vals[pos])
            pos += 1
    res = temp.pop()
    return res, pos


for line in lines:
    parsed = parse(line, [], 0)
    result, _ = evaluate(parsed, 0)
    print('{:.2f}'.format(result))
