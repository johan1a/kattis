#!/usr/bin/python
import sys
from collections import defaultdict

lines = []
for line in sys.stdin:
    lines.append(line.strip())

n, k, r = [int(x) for x in lines[0].split()]
dna = [int(x) for x in lines[1].split()]
minimums = defaultdict(lambda: 0)
for i in range(2, 2 + r):
    (name, minimum) = [int(x) for x in lines[i].split()]
    minimums[name] = minimum

counts = defaultdict(lambda: 0)

satisfied = set()


def find_shortest(dna, start, end):
    length = end - start + 2
    left = start
    right = start

    while right <= end:
        if len(satisfied) < r:
            key = dna[right]
            if key in minimums:
                counts[key] += 1
                if counts[key] == minimums[key]:
                    satisfied.add(key)
        if len(satisfied) == r:
            while len(satisfied) == r:
                key = dna[left]
                if key in minimums:
                    counts[key] -= 1
                    if counts[key] < minimums[key]:
                        satisfied.remove(key)
                left += 1
            length = min(length, right - left + 2)
        right += 1
    return length


i = 0
j = len(dna) - 1
result = find_shortest(dna, i, j)
if result == len(dna) + 1:
    print("impossible")
else:
    print(result)
