#!/usr/bin/python
import sys
from collections import defaultdict

lines = []
for line in sys.stdin:
    lines.append(line.strip())
nbr_tests = int(lines[0])

tests = []
i = 1
while i < len(lines):
    n = int(lines[i])
    test = defaultdict(lambda: 0)
    for j in range(i + 1, i + n + 1):
        test[lines[j].split()[1]] += 1
    tests.append(test)
    i += n + 1

def nbr_combinations(disguises):
    if len(disguises) == 0:
        return 0

    total = 1

    for disguise in disguises:
        # For each category, we can choose to use a single type of that category
        # That gives us disguises[disguise] possibilities
        # -> disguises[disguise]
        # But we can also choose not to use it at all:
        # That gives us an additional possibility
        # -> + 1
        total *= (disguises[disguise] + 1)
    # We remove one for the case where no disguise at all is used, which is not a valid disguise
    # -> -1
    return total - 1


for test in tests:
    print(nbr_combinations(test))
