#!/usr/bin/python
import sys
from collections import defaultdict

lines = []
for line in sys.stdin:
    lines.append(line.strip())

n = int(lines[0])
if n < 1:
    print(0)
    exit()

i = 1
while n >= pow(2 * i - 1, 2):
    n -= pow(2 * i - 1, 2)
    i += 1
print(i-1)
